# Money Transfer Rest API

## Brief
- A RESTful API for money transfers between accounts. This project is a webservice which is supporting concurrent calls. I started to write tests and applied TDD methodology in this project.

## Technologies
### Project
 - *Jetty*
 - *Jersey*
 
### Test
 - *JUnit*
 - *Assertion*
 - *Mockito*
 
## Requirements
 
- *Java 8*
- *Maven*

## How to build and run the application
- After cloning the project from this repository, then run this command

```sh
mvn clean install
```
- Now run this command to run the server

```sh
mvn exec:java
```
- Application starts webserver on http://localhost:8080 by default. 


## Account API

**GET** - `/moneytransfer/accounts`

Sample Response:
```javascript
[
    {
        "accountId": "36",
        "balance": 4,
        "result": "SUCCESSFUL"
    },
    {
        "accountId": "35",
        "balance": 5,
        "result": "SUCCESSFUL"
    }
]
```
---
**POST** - `/moneytransfer/accounts/create/account`

Sample request:
```javascript
{
	"accountId":"35",
	"balance": 4.0
}
```

Sample response:
```javascript
{
    "accountId": "35",
    "balance": 4,
    "result": "SUCCESSFUL"
}
```
---
**GET** - `/moneytransfer/accounts/clear`

Sample response:
```javascript
All cleared!
```

## Transaction API

**POST** - `/moneytransfer/transaction/money/transfer`

Sample request:
```javascript
{
	"sourceAccountId":"35",
	"targetAccountId":"36",
	"transferAmount": 2.0
}
```

Sample response:
```javascript
{
    "transferId": "315325327",
    "transferAmount": 2,
    "sourceAccountId": "35",
    "targetAccountId": "36",
    "transferResult": "SUCCESSFUL"
}
```
**GET** - `/moneytransfer/transaction/transfers`

Sample response:
```javascript
[
    {
        "transferId": "315325327",
        "transferAmount": 2,
        "sourceAccountId": "35",
        "targetAccountId": "36",
        "transferResult": "SUCCESSFUL"
    }
]
```

**GET** - `/moneytransfer/transaction/clear`

Sample response:
```javascript
All cleared!
```

## Exceptions

Duplicated Operation Exception response:
```javascript
Dublicates are not allowed. Account ID 2 already exists.
```

Insufficient Operation Exception response:
```javascript
Money transfer can't be performed because of account balance insufficient!
```

Illegal Operation Exception response:
```javascript
Source Account or Target Account does not exist. 
```
```javascript
Transfer Amount can not be less than 0!
```
