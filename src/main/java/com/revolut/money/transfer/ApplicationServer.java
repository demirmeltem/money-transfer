package com.revolut.money.transfer;

import com.revolut.money.transfer.config.ApplicationServerConfig;

public class ApplicationServer {

    public static void main(String[] args) {
        ApplicationServerConfig.startServer();
    }

}
