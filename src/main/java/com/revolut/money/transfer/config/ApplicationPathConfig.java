package com.revolut.money.transfer.config;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("resources")
class ApplicationPathConfig extends ResourceConfig {

    ApplicationPathConfig() {
        packages("com.revolut.money.transfer");
    }
}
