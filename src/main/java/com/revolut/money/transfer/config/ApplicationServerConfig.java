package com.revolut.money.transfer.config;

import com.revolut.money.transfer.rest.AccountRestControllerImpl;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class ApplicationServerConfig {

    private static Logger logger = Logger.getLogger(ApplicationServerConfig.class);
    private static final int PORT = 8080;
    private static final String CONTEXT_PATH = "/moneytransfer/*";

    public static void startServer() {

        ApplicationPathConfig config = new ApplicationPathConfig();
        Server server = new Server(PORT);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder servletHolder = new ServletHolder(new ServletContainer(config));
        context.addServlet(servletHolder, CONTEXT_PATH);
        servletHolder.setInitParameter("jersey", AccountRestControllerImpl.class.getCanonicalName());

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            logger.error("[ApplicationConfig].[startServer]" + e.getMessage());
            Runtime.getRuntime().exit(1);
        } finally {
            server.destroy();
        }
    }
}
