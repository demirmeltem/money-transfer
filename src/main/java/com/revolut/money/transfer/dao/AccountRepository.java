package com.revolut.money.transfer.dao;

import com.revolut.money.transfer.data.entity.Account;
import com.revolut.money.transfer.data.entity.Transfer;

import java.util.Collection;

public interface AccountRepository {

    public Collection<Account> getAccounts();

    public  Account createAccount(Account account);

    public Account getByAccountId(String id);

    public void clear();

    public void updateAccountBalances(Transfer transfer);
}
