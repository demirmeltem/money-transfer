package com.revolut.money.transfer.dao;

import com.revolut.money.transfer.data.entity.Account;
import com.revolut.money.transfer.data.entity.Transfer;
import com.revolut.money.transfer.exception.DublicateOperationException;
import com.revolut.money.transfer.exception.IllegalOperationException;
import com.revolut.money.transfer.exception.InsufficientOperationException;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class AccountRepositoryImpl implements AccountRepository {

    private static final AccountRepository accountRepository = new AccountRepositoryImpl(new ConcurrentHashMap<>());

    public static AccountRepository getInstance() {
        return accountRepository;
    }

    private final Map<String, Account> accounts;

    public AccountRepositoryImpl(Map<String, Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public Collection<Account> getAccounts() {
        return accounts.values();
    }

    @Override
    public Account getByAccountId(String id) {
        return accounts.get(id);
    }

    @Override
    public Account createAccount(Account account){
        Account createdAccount = accounts.putIfAbsent(account.getAccountId(), account);
        if (createdAccount != null) {
            throw new DublicateOperationException(account.getAccountId());
        }
        return account;
    }

    @Override
    public void clear() {
        accounts.clear();
    }

    @Override
    public void updateAccountBalances(Transfer transfer) {
        Account sourceAccount = accountRepository.getByAccountId(transfer.getSourceAccountId());
        Account targetAccount = accountRepository.getByAccountId(transfer.getTargetAccountId());

        if (!Objects.isNull(sourceAccount) && !Objects.isNull(targetAccount)) {
            if (sourceAccount.getBalance().compareTo(transfer.getTransferAmount()) < 0) {
                throw new InsufficientOperationException();
            }

            sourceAccount.setBalance(sourceAccount.getBalance().subtract(transfer.getTransferAmount()));
            targetAccount.setBalance(targetAccount.getBalance().add(transfer.getTransferAmount()));
        } else {
            throw new IllegalOperationException();
        }
    }

}
