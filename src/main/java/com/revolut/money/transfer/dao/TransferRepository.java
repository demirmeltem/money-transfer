package com.revolut.money.transfer.dao;

import com.revolut.money.transfer.data.entity.Transfer;

import java.util.Collection;

public interface TransferRepository {

    public Collection<Transfer> getTransfers();

    public Transfer getByTransferId(String id);

    public Transfer transfer(Transfer transfer);

    public void clear();
}
