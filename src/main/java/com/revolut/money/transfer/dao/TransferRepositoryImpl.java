package com.revolut.money.transfer.dao;

import com.revolut.money.transfer.data.entity.Transfer;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TransferRepositoryImpl implements TransferRepository {

    private static final TransferRepository transferRepository = new TransferRepositoryImpl(new ConcurrentHashMap<>());

    public static TransferRepository getInstance() {
        return transferRepository;
    }

    private final Map<String, Transfer> transfers;

    public TransferRepositoryImpl(Map<String, Transfer> transfers) {
        this.transfers = transfers;
    }

    @Override
    public Collection<Transfer> getTransfers() {
        return transfers.values();
    }

    @Override
    public Transfer getByTransferId(String id) {
        return transfers.get(id);
    }

    @Override
    public Transfer transfer(Transfer transfer) {
        return transfers.putIfAbsent(transfer.getTransferId(), transfer);
    }

    @Override
    public void clear() {
        transfers.clear();
    }
}
