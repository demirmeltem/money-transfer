package com.revolut.money.transfer.data.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountInputDTO implements Serializable {

    @JsonProperty("accountId")
    private String accountId;

    @JsonProperty(required = true)
    private BigDecimal balance;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
