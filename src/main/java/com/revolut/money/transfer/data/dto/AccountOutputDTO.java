package com.revolut.money.transfer.data.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountOutputDTO implements Serializable {

    @JsonProperty("accountId")
    private String accountId;

    @JsonProperty("balance")
    private BigDecimal balance;

    @JsonProperty("result")
    private String result;


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
