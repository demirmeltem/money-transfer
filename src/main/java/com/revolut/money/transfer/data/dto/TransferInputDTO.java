package com.revolut.money.transfer.data.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransferInputDTO implements Serializable {

    @JsonProperty(required = true)
    private String sourceAccountId;

    @JsonProperty(required = true)
    private BigDecimal transferAmount;

    @JsonProperty(required = true)
    private String targetAccountId;

    public TransferInputDTO() {
        this.transferAmount = BigDecimal.ZERO;
        this.sourceAccountId = "";
        this.targetAccountId = "";
    }

    public TransferInputDTO(BigDecimal transferAmount, String sourceAccountId, String targetAccountId) {
        this.transferAmount = transferAmount;
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
    }

    public String getSourceAccountId() {
        return sourceAccountId;
    }

    public void setSourceAccountId(String sourceAccountId) {
        this.sourceAccountId = sourceAccountId;
    }

    public String getTargetAccountId() {
        return targetAccountId;
    }

    public void setTargetAccountId(String targetAccountId) {
        this.targetAccountId = targetAccountId;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }
}
