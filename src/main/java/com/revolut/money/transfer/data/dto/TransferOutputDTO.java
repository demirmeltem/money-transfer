package com.revolut.money.transfer.data.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransferOutputDTO implements Serializable {

    @JsonProperty("transferId")
    private String transferId;

    @JsonProperty("transferAmount")
    private BigDecimal transferAmount;

    @JsonProperty("sourceAccountId")
    private String sourceAccountId;

    @JsonProperty("targetAccountId")
    private String targetAccountId;

    @JsonProperty("transferResult")
    private String transferResult;

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getSourceAccountId() {
        return sourceAccountId;
    }

    public void setSourceAccountId(String sourceAccountId) {
        this.sourceAccountId = sourceAccountId;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTargetAccountId() {
        return targetAccountId;
    }

    public void setTargetAccountId(String targetAccountId) {
        this.targetAccountId = targetAccountId;
    }

    public String getTransferResult() {
        return transferResult;
    }

    public void setTransferResult(String transferResult) {
        this.transferResult = transferResult;
    }
}
