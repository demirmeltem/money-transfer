package com.revolut.money.transfer.data.entity;

import java.math.BigDecimal;
import java.util.Random;

public class Account {

    private String accountId;

    private BigDecimal balance;

    public Account() {
        this.accountId = String.valueOf(Math.abs(new Random().nextInt()));
        this.balance = null;
    }

    public Account(String balance) {
        this.accountId = String.valueOf(Math.abs(new Random().nextInt()));
        this.balance = new BigDecimal(balance);
    }

    public Account(String accountId, String balance) {
        this.accountId = accountId;
        this.balance = new BigDecimal(balance);
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

}

