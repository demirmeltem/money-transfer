package com.revolut.money.transfer.data.entity;

import java.math.BigDecimal;
import java.util.Random;

public class Transfer {

    private String transferId;

    private BigDecimal transferAmount;

    private String sourceAccountId;

    private String targetAccountId;

    private String transferResult;

    public Transfer() {
        this.transferId = String.valueOf(Math.abs(new Random().nextInt()));
        this.transferAmount = null;
        this.sourceAccountId = null;
        this.targetAccountId = null;
    }

    public Transfer(BigDecimal transferAmount, String sourceAccountId, String targetAccountId) {
        this.transferId = String.valueOf(Math.abs(new Random().nextInt()));
        this.transferAmount = transferAmount;
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getSourceAccountId() {
        return sourceAccountId;
    }

    public void setSourceAccountId(String sourceAccountId) {
        this.sourceAccountId = sourceAccountId;
    }

    public String getTargetAccountId() {
        return targetAccountId;
    }

    public void setTargetAccountId(String targetAccountId) {
        this.targetAccountId = targetAccountId;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getTransferResult() {
        return transferResult;
    }

    public void setTransferResult(String transferResult) {
        this.transferResult = transferResult;
    }

}
