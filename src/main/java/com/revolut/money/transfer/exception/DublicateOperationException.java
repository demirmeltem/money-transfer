package com.revolut.money.transfer.exception;

public class DublicateOperationException extends RuntimeException {

    public DublicateOperationException(String accountId) {
        super("Dublicates are not allowed. Account ID " + accountId + " already exists.");
    }
}
