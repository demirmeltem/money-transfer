package com.revolut.money.transfer.exception;

public class IllegalOperationException extends RuntimeException {

    public IllegalOperationException() {
        super("Source Account or Target Account does not exist. ");
    }

    public IllegalOperationException(String explanation) {
        super("Source Account or Target Account does not exist. ");
    }
}
