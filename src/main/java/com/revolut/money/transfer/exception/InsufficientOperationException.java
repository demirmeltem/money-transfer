package com.revolut.money.transfer.exception;

public class InsufficientOperationException extends RuntimeException {

    public InsufficientOperationException() {
        super("Money transfer can't be performed because of account balance insufficient!");
    }
}
