package com.revolut.money.transfer.mapper;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;
import com.revolut.money.transfer.data.entity.Account;

import java.util.Collection;

public interface AccountMapper {

    Collection<AccountOutputDTO> map(Collection<Account> accounts);

    Account map(AccountInputDTO accountInputDTO);

    AccountOutputDTO map(Account createdAccount);
}
