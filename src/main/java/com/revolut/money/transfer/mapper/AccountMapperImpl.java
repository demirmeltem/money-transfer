package com.revolut.money.transfer.mapper;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;
import com.revolut.money.transfer.data.entity.Account;
import com.revolut.money.transfer.helper.AccountHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class AccountMapperImpl implements AccountMapper {
    @Override
    public Collection<AccountOutputDTO> map(Collection<Account> accounts) {
        Collection<AccountOutputDTO> dtoList = new ArrayList<>();

        if (!Objects.isNull(accounts) && !accounts.isEmpty()) {
            accounts.forEach(account -> {
                AccountOutputDTO accountOutputDTO = new AccountOutputDTO();
                accountOutputDTO.setBalance(account.getBalance());
                accountOutputDTO.setAccountId(account.getAccountId());
                accountOutputDTO.setResult(AccountHelper.SUCCESSFUL);
                dtoList.add(accountOutputDTO);
            });
        }
        return dtoList;
    }

    @Override
    public Account map(AccountInputDTO accountInputDTO) {

        Account account = new Account();
        if (!Objects.isNull(accountInputDTO.getAccountId()) && !accountInputDTO.getAccountId().isEmpty()) {
            account.setAccountId(accountInputDTO.getAccountId());
            account.setBalance(accountInputDTO.getBalance());
        } else {
            account.setBalance(accountInputDTO.getBalance());
        }

        return account;
    }

    @Override
    public AccountOutputDTO map(Account createdAccount) {

        AccountOutputDTO accountOutputDTO = new AccountOutputDTO();
        accountOutputDTO.setBalance(createdAccount.getBalance());
        accountOutputDTO.setAccountId(createdAccount.getAccountId());

        return accountOutputDTO;
    }

}