package com.revolut.money.transfer.mapper;

import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;
import com.revolut.money.transfer.data.entity.Transfer;

import java.util.Collection;

public interface TransferMapper {

    Transfer map(TransferInputDTO transferInputDTO);

    TransferOutputDTO map(Transfer transfered);

    Collection<TransferOutputDTO> map(Collection<Transfer> transferResults);
}
