package com.revolut.money.transfer.mapper;

import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;
import com.revolut.money.transfer.data.entity.Transfer;
import com.revolut.money.transfer.helper.AccountHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class TransferMapperImpl implements TransferMapper {

    @Override
    public Transfer map(TransferInputDTO transferInputDTO) {

        Transfer transfer = new Transfer();
        transfer.setSourceAccountId(transferInputDTO.getSourceAccountId());
        transfer.setTargetAccountId(transferInputDTO.getTargetAccountId());
        transfer.setTransferAmount(transferInputDTO.getTransferAmount());

        return transfer;
    }

    @Override
    public TransferOutputDTO map(Transfer transfered) {

        TransferOutputDTO transferOutputDTO = new TransferOutputDTO();
        transferOutputDTO.setTransferId(transfered.getTransferId());
        transferOutputDTO.setTransferResult(AccountHelper.SUCCESSFUL);
        transferOutputDTO.setTransferAmount(transfered.getTransferAmount());
        transferOutputDTO.setTargetAccountId(transfered.getTargetAccountId());
        transferOutputDTO.setSourceAccountId(transfered.getSourceAccountId());

        return transferOutputDTO;
    }

    @Override
    public Collection<TransferOutputDTO> map(Collection<Transfer> transferResults) {
        Collection<TransferOutputDTO> dtoList = new ArrayList<>();

        if (!Objects.isNull(transferResults) && !transferResults.isEmpty()) {
            transferResults.forEach(transfer -> {
                TransferOutputDTO transferOutputDTO = new TransferOutputDTO();
                transferOutputDTO.setTransferId(transfer.getTransferId());
                transferOutputDTO.setSourceAccountId(transfer.getSourceAccountId());
                transferOutputDTO.setTargetAccountId(transfer.getTargetAccountId());
                transferOutputDTO.setTransferAmount(transfer.getTransferAmount());
                transferOutputDTO.setTransferResult(AccountHelper.SUCCESSFUL);
                dtoList.add(transferOutputDTO);
            });
        }
        return dtoList;
    }
}
