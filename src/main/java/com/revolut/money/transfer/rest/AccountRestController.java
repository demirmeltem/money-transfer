package com.revolut.money.transfer.rest;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;

import java.util.Collection;

public interface AccountRestController {

    public Collection<AccountOutputDTO> getAllAcounts();

    public AccountOutputDTO createAccount(AccountInputDTO accountInputDTO);

    public String clearAccounts();
}
