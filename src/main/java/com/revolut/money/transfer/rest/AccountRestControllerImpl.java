package com.revolut.money.transfer.rest;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;
import com.revolut.money.transfer.service.AccountService;
import com.revolut.money.transfer.service.AccountServiceImpl;
import com.revolut.money.transfer.exception.DublicateOperationException;
import com.revolut.money.transfer.helper.AccountHelper;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collection;


@Path("/accounts")
public class AccountRestControllerImpl implements AccountRestController {

    private static Logger logger = Logger.getLogger(AccountRestControllerImpl.class);

    private AccountService accountService = new AccountServiceImpl();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<AccountOutputDTO> getAllAcounts() {

        Collection<AccountOutputDTO> accountOutputDTO = new ArrayList<>();
        try {
            accountOutputDTO = accountService.getAllAccounts();
        } catch (Exception e) {
            logger.error("[AccountRestControllerImpl].[getAllAcounts]" + e.getMessage());
        }

        return accountOutputDTO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create/account")
    @Override
    public AccountOutputDTO createAccount(AccountInputDTO accountInputDTO) {
        AccountOutputDTO accountOutputDTO = new AccountOutputDTO();
        try {
            accountOutputDTO = accountService.createAccount(accountInputDTO);
            accountOutputDTO.setResult(AccountHelper.SUCCESSFUL);
        } catch (DublicateOperationException e) {
            accountOutputDTO.setResult(e.getMessage());
            logger.error("[AccountRestControllerImpl].[createAccount]" + e.getMessage());
        }
        catch (Exception e) {
            accountOutputDTO.setResult(AccountHelper.FAILED);
            logger.error("[AccountRestControllerImpl].[createAccount]" + e.getMessage());
        }

        return accountOutputDTO;
    }

    @GET
    @Path("/clear")
    @Override
    public String clearAccounts(){
        try {
            accountService.clearAccounts();
        } catch (Exception e) {
            logger.error("[AccountRestControllerImpl].[clearAccounts]" + e.getMessage());
        }
        return "All cleared!";
    }
}
