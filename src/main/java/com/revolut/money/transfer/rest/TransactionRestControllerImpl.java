package com.revolut.money.transfer.rest;

import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;
import com.revolut.money.transfer.service.TransactionService;
import com.revolut.money.transfer.service.TransactionServiceImpl;
import com.revolut.money.transfer.exception.IllegalOperationException;
import com.revolut.money.transfer.exception.InsufficientOperationException;
import com.revolut.money.transfer.helper.AccountHelper;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

@Path("/transaction")
public class TransactionRestControllerImpl implements TransactionRestController {

    private static Logger logger = Logger.getLogger(TransactionRestControllerImpl.class);

    private TransactionService transactionService = new TransactionServiceImpl();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/money/transfer")
    @Override
    public TransferOutputDTO transferMoney(TransferInputDTO transferInputDTO) {

        TransferOutputDTO transferOutputDTO = new TransferOutputDTO();

        try {
            validate(transferInputDTO);
            transferOutputDTO = transactionService.transferMoney(transferInputDTO);
        } catch (IllegalOperationException e) {
            transferOutputDTO.setTransferResult(e.getMessage());
            logger.error("[TransactionRestControllerImpl].[transferMoney]" + e.getMessage());
        } catch (InsufficientOperationException e) {
            transferOutputDTO.setTransferResult(e.getMessage());
            logger.error("[TransactionRestControllerImpl].[transferMoney]" + e.getMessage());
        } catch (Exception e) {
            transferOutputDTO.setTransferResult(AccountHelper.FAILED);
            logger.error("[TransactionRestControllerImpl].[transferMoney]" + e.getMessage());
        }

        return transferOutputDTO;
    }

    @GET
    @Path("/transfers")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Collection<TransferOutputDTO> getAllTransactions() {

        Collection<TransferOutputDTO> transferOutputDTOS = new ArrayList<>();
        try {
            transferOutputDTOS = transactionService.getAllTransactions();
        } catch (Exception e) {
            logger.error("[TransactionRestControllerImpl].[getAllTransactions]" + e.getMessage());
        }

        return transferOutputDTOS;
    }

    @GET
    @Path("/clear")
    @Override
    public String clearAccounts(){
        transactionService.clearAccounts();
        return "All cleared!";
    }

    private void validate(TransferInputDTO transferInputDTO) {

        if(transferInputDTO.getTransferAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalOperationException("Transfer Amount can not be less than 0!");
        }
    }


}
