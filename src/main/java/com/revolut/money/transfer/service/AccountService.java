package com.revolut.money.transfer.service;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;

import java.util.Collection;

public interface AccountService {

    public Collection<AccountOutputDTO> getAllAccounts();

    public AccountOutputDTO createAccount(AccountInputDTO accountInputDTO) ;

    public void clearAccounts();

}
