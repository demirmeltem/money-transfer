package com.revolut.money.transfer.service;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;
import com.revolut.money.transfer.data.entity.Account;
import com.revolut.money.transfer.dao.AccountRepository;
import com.revolut.money.transfer.dao.AccountRepositoryImpl;
import com.revolut.money.transfer.mapper.AccountMapper;
import com.revolut.money.transfer.mapper.AccountMapperImpl;

import java.util.Collection;

public class AccountServiceImpl implements AccountService {

    AccountRepository accountRepository = AccountRepositoryImpl.getInstance();

    AccountMapper accountMapper = new AccountMapperImpl();

    @Override
    public Collection<AccountOutputDTO> getAllAccounts() {
        Collection<Account> createdAccounts = accountRepository.getAccounts();
        Collection<AccountOutputDTO> accountOutputDTO = accountMapper.map(createdAccounts);
        return accountOutputDTO;
    }

    @Override
    public AccountOutputDTO createAccount(AccountInputDTO accountInputDTO) {
        Account account = accountMapper.map(accountInputDTO);
        Account createdAccount = accountRepository.createAccount(account);
        AccountOutputDTO accountOutputDTO = accountMapper.map(createdAccount);

        return accountOutputDTO;
    }

    @Override
    public void clearAccounts() {
        accountRepository.clear();
    }
}
