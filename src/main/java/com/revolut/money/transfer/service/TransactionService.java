package com.revolut.money.transfer.service;

import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;

import java.util.Collection;

public interface TransactionService {

    public TransferOutputDTO transferMoney(TransferInputDTO transferInputDTO);

    public Collection<TransferOutputDTO> getAllTransactions();

    public void clearAccounts();

}
