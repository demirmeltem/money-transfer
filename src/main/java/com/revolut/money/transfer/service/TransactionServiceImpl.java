package com.revolut.money.transfer.service;

import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;
import com.revolut.money.transfer.data.entity.Transfer;
import com.revolut.money.transfer.dao.AccountRepository;
import com.revolut.money.transfer.dao.AccountRepositoryImpl;
import com.revolut.money.transfer.dao.TransferRepository;
import com.revolut.money.transfer.dao.TransferRepositoryImpl;
import com.revolut.money.transfer.mapper.TransferMapper;
import com.revolut.money.transfer.mapper.TransferMapperImpl;

import java.util.Collection;

public class TransactionServiceImpl implements TransactionService {

    private TransferMapper transferMapper = new TransferMapperImpl();

    AccountRepository accountRepository = AccountRepositoryImpl.getInstance();

    TransferRepository transferRepository = TransferRepositoryImpl.getInstance();

    @Override
    public TransferOutputDTO transferMoney(TransferInputDTO transferInputDTO) {
        Transfer transfer = transferMapper.map(transferInputDTO);
        accountRepository.updateAccountBalances(transfer);
        transferRepository.transfer(transfer);
        return transferMapper.map(transfer);
    }

    @Override
    public Collection<TransferOutputDTO> getAllTransactions() {
        Collection<Transfer> transferResults = transferRepository.getTransfers();
        Collection<TransferOutputDTO> transferOutputDTOS = transferMapper.map(transferResults);
        return transferOutputDTOS;
    }

    @Override
    public void clearAccounts() {
        transferRepository.clear();
    }
}
