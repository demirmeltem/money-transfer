package com.revolut.money.transfer.dao;

import com.revolut.money.transfer.data.entity.Account;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositoryImplTest {

    private AccountRepository accountRepository = AccountRepositoryImpl.getInstance();

    @Test
    public void createAccount_shouldAccountSizeGreaterThan0_whenAccountCreatedSuccessfully() {

        Account account = new Account();

        accountRepository.createAccount(account);

        assertThat(accountRepository.getAccounts().size()).isGreaterThan(0);
        assertThat(accountRepository.getByAccountId(account.getAccountId())).isEqualTo(account);
    }

    @Test
    public void createAccount_shouldReturnDublicateOperationError_whenAccountIdDublicated() {

        String accountId = "1";
        Account account = new Account(accountId, "10");
        accountRepository.createAccount(account);
        Account dublicateAccount = new Account(accountId, "20");

        try {
            accountRepository.createAccount(dublicateAccount);
        } catch (Exception e) {
            assertThat(e.getMessage()).isEqualTo("Dublicates are not allowed. Account ID " + accountId + " already exists.");
        }
    }
}
