package com.revolut.money.transfer.dao;

import com.revolut.money.transfer.data.entity.Transfer;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TransferRepositoryImplTest {

    private TransferRepository transferRepository = TransferRepositoryImpl.getInstance();

    @Test
    public void transfer_shouldTransferSizeGreaterThan0_whenTransferredSuccessfully() {

        Transfer transfer = new Transfer();

        transferRepository.transfer(transfer);

        assertThat(transferRepository.getTransfers().size()).isGreaterThan(0);
        assertThat(transferRepository.getByTransferId(transfer.getTransferId())).isEqualTo(transfer);
    }
}
