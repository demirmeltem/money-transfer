package com.revolut.money.transfer.mapper;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;
import com.revolut.money.transfer.data.entity.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class AccountMapperImplTest {

    @InjectMocks
    AccountMapper accountMapper = new AccountMapperImpl();

    @Test
    public void map_shouldMapCollectionAccountToAccountOutputDTOCollection() {

        Collection<Account> accounts = new ArrayList<>();
        accounts.forEach(account -> {
            account.setBalance(ACCOUNT_BALANCE);
            account.setAccountId(ACCOUNT_ID);
        });

        Collection<AccountOutputDTO> accountOutputDTOS = accountMapper.map(accounts);

        accountOutputDTOS.forEach(accountOutputDTO -> {
            assertThat(accountOutputDTO.getAccountId()).isEqualTo(ACCOUNT_ID);
            assertThat(accountOutputDTO.getBalance()).isEqualTo(ACCOUNT_BALANCE);
        });
    }

    @Test
    public void map_shouldMapAccountInputDTOToAccountEntity() {

        AccountInputDTO accountInputDTO = new AccountInputDTO();
        accountInputDTO.setAccountId(ACCOUNT_ID);
        accountInputDTO.setBalance(ACCOUNT_BALANCE);

        Account account = accountMapper.map(accountInputDTO);

        assertThat(account.getBalance()).isEqualTo(ACCOUNT_BALANCE);
        assertThat(account.getAccountId()).isEqualTo(ACCOUNT_ID);

    }

    @Test
    public void map_shouldMapAccountEntityToAccountOutputDTO() {

        Account account = new Account();
        account.setAccountId(ACCOUNT_ID);
        account.setBalance(ACCOUNT_BALANCE);

        AccountOutputDTO accountOutputDTO = accountMapper.map(account);

        assertThat(accountOutputDTO.getBalance()).isEqualTo(ACCOUNT_BALANCE);
        assertThat(accountOutputDTO.getAccountId()).isEqualTo(ACCOUNT_ID);
    }

    private static final BigDecimal ACCOUNT_BALANCE = BigDecimal.ONE;
    private static final String ACCOUNT_ID = "1";
    private static final String RESULT = "SUCCESSFUL";

}
