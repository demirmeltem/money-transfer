package com.revolut.money.transfer.mapper;

import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;
import com.revolut.money.transfer.data.entity.Transfer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TransferMapperImplTest {

    @InjectMocks
    TransferMapper transferMapper = new TransferMapperImpl();

    @Test
    public void map_shouldMapTransferInputDTOToTransferEntity() {

        TransferInputDTO transferInputDTO = new TransferInputDTO();
        transferInputDTO.setTransferAmount(TRANSFER_AMOUNT);
        transferInputDTO.setSourceAccountId(SOURCE_ACCOUNT_ID);
        transferInputDTO.setTargetAccountId(TARGET_ACCOUNT_ID);

        Transfer transfer = transferMapper.map(transferInputDTO);

        assertThat(transfer.getTransferAmount()).isEqualTo(TRANSFER_AMOUNT);
        assertThat(transfer.getSourceAccountId()).isEqualTo(SOURCE_ACCOUNT_ID);
        assertThat(transfer.getTargetAccountId()).isEqualTo(TARGET_ACCOUNT_ID);
    }

    @Test
    public void map_shouldMapTransferEntityToTransferOutputDTO() {

        Transfer transfer = new Transfer();
        transfer.setTransferResult(TRANSFER_RESULT);
        transfer.setSourceAccountId(SOURCE_ACCOUNT_ID);
        transfer.setTargetAccountId(TARGET_ACCOUNT_ID);
        transfer.setTransferAmount(TRANSFER_AMOUNT);
        transfer.setTransferId(TRANSFER_ID);

        TransferOutputDTO transferOutputDTO = transferMapper.map(transfer);

        assertThat(transferOutputDTO.getTransferResult()).isEqualTo(TRANSFER_RESULT);
        assertThat(transferOutputDTO.getSourceAccountId()).isEqualTo(SOURCE_ACCOUNT_ID);
        assertThat(transferOutputDTO.getTargetAccountId()).isEqualTo(TARGET_ACCOUNT_ID);
        assertThat(transferOutputDTO.getTransferAmount()).isEqualTo(TRANSFER_AMOUNT);
        assertThat(transferOutputDTO.getTransferId()).isEqualTo(TRANSFER_ID);
    }

    @Test
    public void map_shouldMapTransferCollectionToTransferOutputDTOCollection() {

        Collection<Transfer> transfers = new ArrayList<>();
        transfers.forEach(transfer -> {
            transfer.setTransferId(TRANSFER_ID);
            transfer.setTransferAmount(TRANSFER_AMOUNT);
            transfer.setTargetAccountId(SOURCE_ACCOUNT_ID);
            transfer.setSourceAccountId(TARGET_ACCOUNT_ID);
            transfer.setTransferResult(TRANSFER_RESULT);
        });

        Collection<TransferOutputDTO> transferOutputDTOS = transferMapper.map(transfers);
        transferOutputDTOS.forEach(transferOutputDTO -> {
            assertThat(transferOutputDTO.getTransferId()).isEqualTo(TRANSFER_ID);
            assertThat(transferOutputDTO.getTransferAmount()).isEqualTo(TRANSFER_AMOUNT);
            assertThat(transferOutputDTO.getTargetAccountId()).isEqualTo(TARGET_ACCOUNT_ID);
            assertThat(transferOutputDTO.getSourceAccountId()).isEqualTo(SOURCE_ACCOUNT_ID);
            assertThat(transferOutputDTO.getTransferResult()).isEqualTo(TRANSFER_RESULT);
        });

    }

    private static final BigDecimal TRANSFER_AMOUNT = BigDecimal.ONE;
    private static final String TARGET_ACCOUNT_ID = "1";
    private static final String SOURCE_ACCOUNT_ID = "2";
    private static final String TRANSFER_RESULT = "SUCCESSFUL";
    private static final String TRANSFER_ID = "2";
}
