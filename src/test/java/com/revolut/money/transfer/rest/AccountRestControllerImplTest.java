package com.revolut.money.transfer.rest;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;
import com.revolut.money.transfer.service.AccountService;
import com.revolut.money.transfer.exception.DublicateOperationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountRestControllerImplTest {

    @InjectMocks
    AccountRestController accountRestController = new AccountRestControllerImpl();

    @Mock
    AccountService accountService;

    @Test
    public void getAllAcounts_shouldReturnAccountOutputDTO_whenAccountServiceThrowsNoException() {

        when(accountService.getAllAccounts()).thenReturn(new ArrayList<>());

        Collection<AccountOutputDTO> accountOutputDTO = accountRestController.getAllAcounts();

        assertThat(accountOutputDTO).isNotNull();
        verify(accountService, atLeastOnce()).getAllAccounts();
    }

    @Test(expected = Exception.class)
    public void getAllAccounts_shouldReturnFailedResult_whenAccountServiceThrowsException() {

        doThrow(new Exception()).when(accountService).getAllAccounts();

        Collection<AccountOutputDTO> accountOutputDTO = accountRestController.getAllAcounts();

        assertThat(accountOutputDTO).isNotNull();
        accountOutputDTO.forEach(accountOutputDTO1 -> {
            assertThat(accountOutputDTO1.getResult()).isEqualTo(FAILED);
        });
    }

    @Test
    public void createAccount_shouldReturnAccountOutputDTO_whenAccountServiceThrowsNoException() {

        when(accountService.createAccount(any(AccountInputDTO.class))).thenReturn(new AccountOutputDTO());

        AccountOutputDTO accountOutputDTO = accountRestController.createAccount(any(AccountInputDTO.class));

        assertThat(accountOutputDTO).isNotNull();
        verify(accountService, atLeastOnce()).createAccount(any(AccountInputDTO.class));
    }

    @Test(expected = Exception.class)
    public void createAccount_shouldReturnFailedResult_whenAccountServiceThrowsException() {

        doThrow(new Exception()).when(accountService).createAccount(any(AccountInputDTO.class));

        AccountOutputDTO accountOutputDTO = accountRestController.createAccount(any(AccountInputDTO.class));

        assertThat(accountOutputDTO).isNotNull();
        assertThat(accountOutputDTO.getResult()).isEqualTo(FAILED);
    }

    @Test
    public void createAccount_shouldReturnDublicateErrorResult_whenAccountServiceThrowsDublicateOperationException() {

        String accountId = "1";

        doThrow(new DublicateOperationException(accountId)).when(accountService).createAccount(any(AccountInputDTO.class));
        AccountOutputDTO accountOutputDTO = accountRestController.createAccount(any(AccountInputDTO.class));

        assertThat(accountOutputDTO).isNotNull();
        assertThat(accountOutputDTO.getResult()).isEqualTo("Dublicates are not allowed. Account ID " + accountId + " already exists.");
    }

    private String FAILED = "FAILED";
}
