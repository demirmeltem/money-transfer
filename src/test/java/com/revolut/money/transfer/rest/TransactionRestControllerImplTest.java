package com.revolut.money.transfer.rest;

import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;
import com.revolut.money.transfer.service.TransactionService;
import com.revolut.money.transfer.exception.IllegalOperationException;
import com.revolut.money.transfer.exception.InsufficientOperationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionRestControllerImplTest {

    @InjectMocks
    TransactionRestController transactionRestController = new TransactionRestControllerImpl();

    @Mock
    TransactionService transactionService;

    @Test
    public void transferMoney_shouldReturnTransferOutputDTO_whenTransactionServiceThrowsNoException() {

        TransferInputDTO transferInputDTO = new TransferInputDTO();
        transferInputDTO.setTransferAmount(BigDecimal.ONE);

        when(transactionService.transferMoney(any(TransferInputDTO.class))).thenReturn(new TransferOutputDTO());
        TransferOutputDTO transferOutputDTO = transactionRestController.transferMoney(transferInputDTO);

        assertThat(transferOutputDTO).isNotNull();
        verify(transactionService, atLeastOnce()).transferMoney(any(TransferInputDTO.class));
    }

    @Test(expected = Exception.class)
    public void transferMoney_shouldReturnFailedResult_whenTransactionServiceThrowsException() {

        doThrow(new Exception()).when(transactionService).transferMoney(any(TransferInputDTO.class));
        TransferOutputDTO transferOutputDTO  = transactionRestController.transferMoney(new TransferInputDTO());

        assertThat(transferOutputDTO).isNotNull();
        assertThat(transferOutputDTO.getTransferResult()).isEqualTo(FAILED);
    }

    @Test
    public void transferMoney_shouldReturnIllegalOperationErrorMessage_whenTransactionServiceThrowsException() {

        TransferInputDTO transferInputDTO = new TransferInputDTO();
        transferInputDTO.setTransferAmount(BigDecimal.ONE);

        doThrow(new IllegalOperationException()).when(transactionService).transferMoney(any(TransferInputDTO.class));
        TransferOutputDTO transferOutputDTO  = transactionRestController.transferMoney(transferInputDTO);

        assertThat(transferOutputDTO).isNotNull();
        assertThat(transferOutputDTO.getTransferResult()).isEqualTo("Source Account or Target Account does not exist. ");
    }

    @Test
    public void transferMoney_shouldReturnInsufficientOperationErrorMessage_whenTransactionServiceThrowsException() {

        TransferInputDTO transferInputDTO = new TransferInputDTO();
        transferInputDTO.setTransferAmount(BigDecimal.ONE);

        doThrow(new InsufficientOperationException()).when(transactionService).transferMoney(any(TransferInputDTO.class));
        TransferOutputDTO transferOutputDTO  = transactionRestController.transferMoney(transferInputDTO);

        assertThat(transferOutputDTO).isNotNull();
        assertThat(transferOutputDTO.getTransferResult()).isEqualTo("Money transfer can't be performed because of account balance insufficient!");
    }


    @Test
    public void getAllTransactions_shouldReturnTransferOutputDTO_whenTransactionServiceThrowsNoException() {

        when(transactionService.getAllTransactions()).thenReturn(new ArrayList<>());
        Collection<TransferOutputDTO> accountOutputDTO = transactionRestController.getAllTransactions();

        assertThat(accountOutputDTO).isNotNull();
        verify(transactionService, atLeastOnce()).getAllTransactions();
    }

    @Test(expected = Exception.class)
    public void getAllTransactions_shouldReturnFailedResult_whenAccountServiceThrowsException() {

        doThrow(new Exception()).when(transactionService).getAllTransactions();

        Collection<TransferOutputDTO> transferOutputDTOS = transactionRestController.getAllTransactions();

        assertThat(transferOutputDTOS).isNotNull();
    }

    private String FAILED = "FAILED";
}
