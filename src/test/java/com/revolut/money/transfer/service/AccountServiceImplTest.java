package com.revolut.money.transfer.service;

import com.revolut.money.transfer.data.dto.AccountInputDTO;
import com.revolut.money.transfer.data.dto.AccountOutputDTO;
import com.revolut.money.transfer.data.entity.Account;
import com.revolut.money.transfer.dao.AccountRepository;
import com.revolut.money.transfer.mapper.AccountMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();

    @Mock
    private AccountMapper accountMapper;

    @Mock
    private AccountRepository accountRepository;

    @Test
    public void getAllAcounts_shouldInvokeAccountMapper_whenAccountRepositoryThrowsNoException() {

        when(accountRepository.getAccounts()).thenReturn(new ArrayList<>());
        when(accountMapper.map(new ArrayList<>())).thenReturn(new ArrayList<>());

        Collection<AccountOutputDTO> accountOutputDTOS =  accountService.getAllAccounts();

        assertThat(accountOutputDTOS).isNotNull();
        accountOutputDTOS.forEach(accountOutputDTO -> {
            assertThat(accountOutputDTO.getResult()).isEqualTo(SUCCESSFUL);
        });
        verify(accountMapper,atLeastOnce()).map(new ArrayList<>());
        verify(accountRepository,atLeastOnce()).getAccounts();
    }

    @Test(expected = Exception.class)
    public void getAllAcounts_shouldNotInvokeAccountMapper_whenAccountRepositoryThrowsException() {

        doThrow(new Exception()).when(accountRepository).getAccounts();

        Collection<AccountOutputDTO> accountOutputDTOS =  accountService.getAllAccounts();

        assertThat(accountOutputDTOS).isNotNull();
        accountOutputDTOS.forEach(accountOutputDTO -> {
            assertThat(accountOutputDTO.getResult()).isEqualTo(FAILED);
        });
        verify(accountMapper,never()).map(new ArrayList<>());
        verify(accountRepository,atLeastOnce()).getAccounts();
    }

    @Test
    public void createAccount_shouldInvokeAccountMapper_whenAccountRepositoryThrowsNoException() {

        when(accountMapper.map(any(AccountInputDTO.class))).thenReturn(new Account());
        when(accountRepository.createAccount(any(Account.class))).thenReturn(new Account());
        when(accountMapper.map(any(Account.class))).thenReturn(new AccountOutputDTO());

        AccountOutputDTO accountOutputDTO =  accountService.createAccount(new AccountInputDTO());

        assertThat(accountOutputDTO).isNotNull();
        verify(accountMapper,atLeastOnce()).map(any(AccountInputDTO.class));
        verify(accountMapper,atLeastOnce()).map(any(Account.class));
        verify(accountRepository,atLeastOnce()).createAccount(any(Account.class));
    }

    @Test(expected = Exception.class)
    public void createAccount_shouldNotInvokeAccountMapper_whenAccountRepositoryThrowsException() {

        when(accountMapper.map(any(AccountInputDTO.class))).thenReturn(new Account());
        doThrow(new Exception()).when(accountRepository).createAccount(any(Account.class));

        AccountOutputDTO accountOutputDTO =  accountService.createAccount(new AccountInputDTO());

        assertThat(accountOutputDTO).isNotNull();
        verify(accountMapper,atLeastOnce()).map(any(AccountInputDTO.class));
        verify(accountMapper,never()).map(any(Account.class));
        verify(accountRepository,never()).createAccount(any(Account.class));
    }

    private String FAILED = "FAILED";
    private String SUCCESSFUL = "SUCCESSFUL";

}
