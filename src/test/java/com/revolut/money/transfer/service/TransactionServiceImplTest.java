package com.revolut.money.transfer.service;

import com.revolut.money.transfer.dao.AccountRepository;
import com.revolut.money.transfer.dao.AccountRepositoryImpl;
import com.revolut.money.transfer.dao.TransferRepository;
import com.revolut.money.transfer.data.dto.TransferInputDTO;
import com.revolut.money.transfer.data.dto.TransferOutputDTO;
import com.revolut.money.transfer.data.entity.Account;
import com.revolut.money.transfer.data.entity.Transfer;
import com.revolut.money.transfer.mapper.AccountMapper;
import com.revolut.money.transfer.mapper.TransferMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTest {

    @InjectMocks
    private TransactionService transactionService = new TransactionServiceImpl();

    @Mock
    private TransferRepository transferRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private TransferMapper transferMapper;

    @Mock
    private AccountMapper accountMapper;

    @Test
    public void getAllTransactions_shouldInvokeTransferMapper_whenTransferRepositoryThrowsNoException() {

        when(transferRepository.getTransfers()).thenReturn(new ArrayList<>());
        when(transferMapper.map(new ArrayList<>())).thenReturn(new ArrayList<>());

        Collection<TransferOutputDTO> transferOutputDTOS =  transactionService.getAllTransactions();

        assertThat(transferOutputDTOS).isNotNull();
        transferOutputDTOS.forEach(transferOutputDTO -> {
            assertThat(transferOutputDTO.getTransferResult()).isEqualTo(SUCCESSFUL);
        });
        verify(transferMapper,atLeastOnce()).map(new ArrayList<>());
        verify(transferRepository,atLeastOnce()).getTransfers();
    }

    @Test(expected = Exception.class)
    public void getAllTransactions_shouldNotInvokeTransferMapper_whenTransferRepositoryThrowsException() {

        doThrow(new Exception()).when(transferRepository).getTransfers();

        Collection<TransferOutputDTO> transferOutputDTOS =  transactionService.getAllTransactions();

        assertThat(transferOutputDTOS).isNotNull();
        transferOutputDTOS.forEach(transferOutputDTO -> {
            assertThat(transferOutputDTO.getTransferResult()).isEqualTo(FAILED);
        });
        verify(transferMapper,never()).map(new ArrayList<>());
        verify(transferRepository,atLeastOnce()).getTransfers();
    }

    @Test
    public void transferMoney_shouldInvokeTransferMapper_whenTransferRepositoryThrowsNoException() {

        when(transferMapper.map(any(TransferInputDTO.class))).thenReturn(new Transfer());
        when(transferRepository.transfer(any(Transfer.class))).thenReturn(new Transfer());
        when(transferMapper.map(any(Transfer.class))).thenReturn(new TransferOutputDTO());

        TransferOutputDTO transferOutputDTO = transactionService.transferMoney(new TransferInputDTO());

        assertThat(transferOutputDTO).isNotNull();
        verify(transferMapper, atLeastOnce()).map(any(TransferInputDTO.class));
        verify(transferRepository, atLeastOnce()).transfer(any(Transfer.class));
        verify(transferMapper, atLeastOnce()).map(any(Transfer.class));

    }

    @Test(expected = Exception.class)
    public void transferMoney_shouldNotInvokeTransferMapper_whenTransferRepositoryThrowsException() {

        when(transferMapper.map(any(TransferInputDTO.class))).thenReturn(new Transfer());
        doThrow(new Exception()).when(transferRepository).transfer(any(Transfer.class));
        when(transferMapper.map(any(Transfer.class))).thenReturn(new TransferOutputDTO());

        TransferOutputDTO transferOutputDTO = transactionService.transferMoney(new TransferInputDTO());

        assertThat(transferOutputDTO).isNotNull();
        verify(transferMapper, atLeastOnce()).map(any(TransferInputDTO.class));
        verify(transferRepository, atLeastOnce()).transfer(any(Transfer.class));
        verify(transferMapper, never()).map(any(Transfer.class));
    }


    private String FAILED = "FAILED";
    private String SUCCESSFUL = "SUCCESSFUL";
}
